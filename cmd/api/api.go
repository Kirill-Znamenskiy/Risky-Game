package main

import (
	"context"

	"github.com/rs/zerolog/log"

	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/app"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/config"
)

var prvGitShowVersion string

func main() {
	ctx := context.Background()

	cfg, err := config.NewAPIConfig(ctx, prvGitShowVersion)
	if err != nil {
		log.Fatal().Err(err).Msg("config.NewAPIConfig error")
	}

	log.Info().Any("APIConfig", cfg).Send()

	err = app.RunAPI(ctx, cfg)
	if err != nil {
		log.Fatal().Err(err).Msg("app.RunAPI error")
	}
}
