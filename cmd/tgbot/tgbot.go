package main

import (
	"context"

	"github.com/rs/zerolog/log"

	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/app"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/config"
)

var prvGitShowVersion string

func main() {

	ctx := context.Background()

	cfg, err := config.NewTgBotConfig(ctx, prvGitShowVersion)
	if err != nil {
		log.Fatal().Err(err).Msg("config.NewTgBotConfig error")
	}

	log.Info().Any("TgBotConfig", cfg).Send()

	err = app.RunTgBot(ctx, cfg)
	if err != nil {
		log.Fatal().Err(err).Msg("app.RunTgBot error")
	}
}
