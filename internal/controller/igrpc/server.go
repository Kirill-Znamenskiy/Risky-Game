package igrpc

import (
	"context"
	"net/http"

	grpc_recovery "github.com/grpc-ecosystem/go-grpc-middleware/recovery"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/encoding/protojson"

	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/controller/igrpc/gen"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/usecase"
)

func NewServer(ctx context.Context,
	mainUseCase usecase.MainUseCase,
	playUseCase usecase.PlayUseCase,
) (
	grpcServer *grpc.Server,
	gwMux *runtime.ServeMux,
	err error,
) {

	lcMainServiceImplementation := prvNewMainServiceServer(mainUseCase)
	lcPlayServiceImplementation := prvNewPlayServiceServer(playUseCase)

	grpcServer = createGrpcServer()
	gen.RegisterMainServiceServer(grpcServer, lcMainServiceImplementation)
	gen.RegisterPlayServiceServer(grpcServer, lcPlayServiceImplementation)

	gwMux = createGwMux()
	err = gen.RegisterMainServiceHandlerServer(ctx, gwMux, lcMainServiceImplementation)
	if err != nil {
		return nil, nil, err
	}
	err = gen.RegisterPlayServiceHandlerServer(ctx, gwMux, lcPlayServiceImplementation)
	if err != nil {
		return nil, nil, err
	}

	return grpcServer, gwMux, err
}

func createGrpcServer() *grpc.Server {

	grpcRecoveryOpts := []grpc_recovery.Option{
		grpc_recovery.WithRecoveryHandler(func(p any) (err error) {
			return status.Errorf(codes.Unknown, "panic triggered: %v", p)
		}),
	}
	return grpc.NewServer(
		grpc.ChainUnaryInterceptor(
			grpc_recovery.UnaryServerInterceptor(grpcRecoveryOpts...),
		),
		grpc.ChainStreamInterceptor(
			grpc_recovery.StreamServerInterceptor(grpcRecoveryOpts...),
		),
	)
}

func createGwMux() *runtime.ServeMux {
	return runtime.NewServeMux(

		runtime.WithMarshalerOption(runtime.MIMEWildcard, &runtime.JSONPb{
			UnmarshalOptions: protojson.UnmarshalOptions{
				DiscardUnknown: false,
			},
			MarshalOptions: protojson.MarshalOptions{
				UseProtoNames:   true,
				EmitUnpopulated: true,
				UseEnumNumbers:  true,
			},
		}),

		runtime.WithErrorHandler(runtime.DefaultHTTPErrorHandler),
		runtime.WithRoutingErrorHandler(func(ctx context.Context, mux *runtime.ServeMux, marshaler runtime.Marshaler, w http.ResponseWriter, r *http.Request, httpStatus int) {
			sterr := status.Error(codes.Internal, "Unexpected routing error")
			switch httpStatus {
			case http.StatusMethodNotAllowed:
				fallthrough
			case http.StatusBadRequest:
				sterr = status.Error(codes.InvalidArgument, http.StatusText(http.StatusBadRequest))
			case http.StatusNotFound:
				sterr = status.Error(codes.NotFound, http.StatusText(httpStatus))
			}
			runtime.DefaultHTTPErrorHandler(ctx, mux, marshaler, w, r, sterr)
		}),
	)
}
