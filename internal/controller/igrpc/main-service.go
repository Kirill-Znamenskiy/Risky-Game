package igrpc

import (
	"context"

	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/controller/igrpc/gen"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/usecase"
)

type prvMainServiceServerImplementation struct {
	gen.UnimplementedMainServiceServer
	mainUseCase usecase.MainUseCase
}

func prvNewMainServiceServer(mainUseCase usecase.MainUseCase) (ret *prvMainServiceServerImplementation) {
	return &prvMainServiceServerImplementation{
		mainUseCase: mainUseCase,
	}
}

func (this *prvMainServiceServerImplementation) GetVersion(_ context.Context, _ *gen.Empty) (resp *gen.GetVersionResponse, err error) {
	resp = &gen.GetVersionResponse{
		Version: this.mainUseCase.GetVersion(),
	}
	return resp, err
}
