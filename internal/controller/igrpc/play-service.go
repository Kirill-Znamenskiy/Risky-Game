package igrpc

import (
	"context"

	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/controller/igrpc/gen"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/entity"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/usecase"
)

type prvPlayServiceServerImplementation struct {
	gen.UnimplementedPlayServiceServer
	playUseCase usecase.PlayUseCase
}

func prvNewPlayServiceServer(playUseCase usecase.PlayUseCase) (ret *prvPlayServiceServerImplementation) {
	return &prvPlayServiceServerImplementation{
		playUseCase: playUseCase,
	}
}

func (this *prvPlayServiceServerImplementation) GetAllChoices(_ context.Context, _ *gen.Empty) (resp *gen.GetAllChoicesResponse, err error) {
	resp = new(gen.GetAllChoicesResponse)
	allChoices := this.playUseCase.GetAllChoices()
	resp.Choices = make([]*gen.Choice, 0, len(allChoices))
	for _, choice := range allChoices {
		resp.Choices = append(resp.Choices, &gen.Choice{
			Id:   gen.ChoiceID(choice.ID),
			Name: choice.Name,
		})
	}
	return resp, err
}
func (this *prvPlayServiceServerImplementation) GetRandomChoice(ctx context.Context, _ *gen.Empty) (resp *gen.GetRandomChoiceResponse, err error) {
	randomChoice, err := this.playUseCase.GetRandomChoice(ctx)
	if err != nil {
		return nil, err
	}
	resp = &gen.GetRandomChoiceResponse{
		Choice: &gen.Choice{
			Id:   gen.ChoiceID(randomChoice.ID),
			Name: randomChoice.Name,
		},
	}
	return resp, err
}
func (this *prvPlayServiceServerImplementation) Play(ctx context.Context, req *gen.PlayRequest) (resp *gen.PlayResponse, err error) {
	playerChoice, computerChoice, result, err := this.playUseCase.Play(ctx, entity.ChoiceID(req.Player))
	if err != nil {
		return nil, err
	}
	resp = &gen.PlayResponse{
		Player:   gen.ChoiceID(playerChoice.ID),
		Computer: gen.ChoiceID(computerChoice.ID),
		Result:   int32(result.Val),
		Results:  result.Name,
	}
	return resp, err
}
