package tgbot

import (
	"context"
	"time"

	tg "gopkg.in/telebot.v3"

	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/usecase"
)

func NewTgBot(ctx context.Context,
	token string,
	pollTimeout time.Duration,
	mainUseCase usecase.MainUseCase,
	playUseCase usecase.PlayUseCase,
) (
	tgBot *tg.Bot,
	err error,
) {

	tgBot, err = tg.NewBot(tg.Settings{
		Token:  token,
		Poller: &tg.LongPoller{Timeout: pollTimeout},
	})
	if err != nil {
		return nil, err
	}

	handlers := prvNewHandlers(tgBot, mainUseCase, playUseCase)

	err = handlers.apply()
	if err != nil {
		return nil, err
	}

	return tgBot, nil
}
