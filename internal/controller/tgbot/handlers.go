package tgbot

import (
	"context"
	"fmt"
	"log"
	"math/rand"
	"strconv"
	"strings"

	tg "gopkg.in/telebot.v3"

	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/entity"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/usecase"
)

type prvHandlers struct {
	tgBot            *tg.Bot
	mainUseCase      usecase.MainUseCase
	playUseCase      usecase.PlayUseCase
	allTgBotCommands []tg.Command
}

func prvNewHandlers(tgBot *tg.Bot, mainUseCase usecase.MainUseCase, playUseCase usecase.PlayUseCase) *prvHandlers {
	ret := &prvHandlers{
		tgBot:       tgBot,
		mainUseCase: mainUseCase,
		playUseCase: playUseCase,
	}
	ret.allTgBotCommands = []tg.Command{
		{Text: "play", Description: "play a game"},
		{Text: "choice", Description: "get one randomly generated choice"},
		{Text: "choices", Description: "see all available choices"},
		{Text: "rules", Description: "see game rules"},

		{Text: "help", Description: "see help(start) message"},
		{Text: "start", Description: "see start(help) message"},
		{Text: "version", Description: "see current app version in form of git show"},
	}
	return ret
}

func (hs *prvHandlers) apply() (err error) {
	err = hs.tgBot.SetCommands(hs.allTgBotCommands)
	if err != nil {
		return err
	}

	startHandlerFunc := hs.makeStartHandlerFunc()
	hs.tgBot.Handle("/help", startHandlerFunc)
	hs.tgBot.Handle("/start", startHandlerFunc)

	hs.tgBot.Handle("/version", hs.makeVersionHandlerFunc())

	hs.tgBot.Handle("/rules", hs.makeRulesHandlerFunc())
	hs.tgBot.Handle("/choice", hs.makeChoiceHandlerFunc())
	hs.tgBot.Handle("/choices", hs.makeChoicesHandlerFunc())

	playHandlerFunc := hs.makePlayHandlerFunc()
	hs.tgBot.Handle("/play", playHandlerFunc)

	choice, err := entity.FindChoice(entity.Rock)
	if err != nil {
		return err
	}
	choiceBtn := makePlayBtn(choice)
	hs.tgBot.Handle(&choiceBtn, playHandlerFunc)

	return nil
}

func (hs *prvHandlers) makeStartHandlerFunc() tg.HandlerFunc {
	return func(tgc tg.Context) (err error) {
		msg := ""
		msg += "```"
		msg += "\nHello!"
		msg += "\n" + `Here you can play a really Risky Game "Rock Scissors Paper Lizard Spock"!`
		msg += "\n"
		msg += "\n" + "Send me one of these commands:"
		msg += "```"
		commands, err := hs.tgBot.Commands()
		if err != nil {
			return
		}
		for _, command := range commands {
			msg += "\n" + `\/` + command.Text + ` \- ` + "`to " + command.Description + "`"
		}
		return tgc.Send(msg, tg.ModeMarkdownV2)
	}
}

func (hs *prvHandlers) makeRulesHandlerFunc() tg.HandlerFunc {
	return func(tgc tg.Context) (err error) {
		msg := ""
		msg += "```"
		msg += "\nIn short, the rules of this Risky Game are:"
		msg += "\n- Scissors cuts Paper"
		msg += "\n- Paper covers Rock"
		msg += "\n- Rock crushes Lizard"
		msg += "\n- Lizard poisons Spock"
		msg += "\n- Spock smashes Scissors"
		msg += "\n- Scissors decapitates Lizard"
		msg += "\n- Lizard eats Paper"
		msg += "\n- Paper disproves Spock"
		msg += "\n- Spock vaporizes Rock"
		msg += "\n- (and as it always has) Rock crushes Scissors"
		msg += "\n"
		msg += "\n" + "More detailed rules ```[here](http://www.samkass.com/theories/RPSSL.html)"
		return tgc.Send(msg, tg.NoPreview, tg.ModeMarkdownV2)
	}
}

func (hs *prvHandlers) makeVersionHandlerFunc() tg.HandlerFunc {
	return func(tgc tg.Context) (err error) {
		msg := ""
		msg += "```"
		msg += "\n" + "My version:"
		msg += "\n" + hs.mainUseCase.GetVersion()
		msg += "\n"
		msg += "\n" + "That's all I can tell about myself, staying within my NDA restrictions."
		msg += "```"
		return tgc.Send(msg, tg.ModeMarkdownV2)
	}
}
func (hs *prvHandlers) makeChoicesHandlerFunc() tg.HandlerFunc {
	return func(tgc tg.Context) (err error) {
		allChoices := hs.playUseCase.GetAllChoices()
		msg := ""
		msg += "```"
		msg += "\n" + fmt.Sprintf("There are %d choices available:", len(allChoices))
		for _, ch := range allChoices {
			msg += "\n" + makeChoiceLine(ch, true)
		}
		msg += "\n"
		msg += "\n" + "If you want to play a round, just make your choice below 😉"
		msg += "```"
		return tgc.Send(msg, tg.ModeMarkdownV2, makeGoRoundReplyMarkup(tgc.Bot(), allChoices))
	}
}
func (hs *prvHandlers) makeChoiceHandlerFunc() tg.HandlerFunc {
	return func(tgc tg.Context) (err error) {
		msg := ""
		msg += "```"
		msg += "\n" + "Randomly generated, especially for you, choice:"
		ch, err := hs.playUseCase.GetRandomChoice(context.TODO())
		if err != nil {
			msg += "\n !!! " + err.Error() + " !!! "
		} else {
			msg += "\n" + makeChoiceLine(ch, false)
		}
		msg += "\n"
		msg += "\n" + "To play a real round of Risky Game, just make your choice below 😉"
		msg += "```"
		return tgc.Send(msg, tg.ModeMarkdownV2, makeGoRoundReplyMarkup(tgc.Bot(), hs.playUseCase.GetAllChoices()))
	}
}
func (hs *prvHandlers) makePlayHandlerFunc() tg.HandlerFunc {
	return func(tgc tg.Context) (err error) {
		msg := ""
		msg += "```"

		playData := tgc.Data()
		if playData != "" {
			playerChoiceID, err := strconv.Atoi(playData)
			if err != nil {
				return err
			}
			playerChoice, computerChoice, result, err := hs.playUseCase.Play(context.TODO(), entity.ChoiceID(playerChoiceID))
			if err != nil {
				return err
			}
			playerChoiceLine := makeChoiceLine(playerChoice, false)
			computerChoiceLine := makeChoiceLine(computerChoice, false)
			msg += "\n" + "The game took place!"
			msg += "\n" + "My choice: " + computerChoiceLine
			msg += "\n" + "Your choice: " + playerChoiceLine
			if result.Val == entity.Win {
				msg += "\n" + "Results:"
				msg += "\n" + playerChoiceLine + " " + actWords[playerChoice.ID][computerChoice.ID] + " " + computerChoiceLine + ", so"
				ILoseResults := [...]string{
					"I lose 😕",
					"I loose 😣",
					"I loose 😤",
				}
				YouWinResults := [...]string{
					"You win, congratulations 🎉",
					"You win, congratulations 👏🏻",
				}
				msg += "\n" + YouWinResults[rand.Intn(len(YouWinResults))]
				msg += "\n" + ILoseResults[rand.Intn(len(ILoseResults))]
				msg += "\n"
				playAgainPhrases := [...]string{
					"This world is so unfair to me, give me a chance to recoup!",
					"There is no justice in this world, lets play again!",
				}
				msg += "\n" + playAgainPhrases[rand.Intn(len(playAgainPhrases))]
			} else if result.Val == entity.Lose {
				msg += "\n" + "Results:"
				msg += "\n" + computerChoiceLine + " " + actWords[computerChoice.ID][playerChoice.ID] + " " + playerChoiceLine + ", so"
				msg += "\n" + "I win! 😏"
				msg += "\n" + "You lose 🤷🏼‍♂️"
				msg += "\n"
				msg += "\n" + "Don't worry, you'll be lucky next round 😉"
			} else if result.Val == entity.Tie {
				msg += "\n" + "Its tie! We need replay!"
			} else {
				log.Println("Something strange!")
				msg += "\n" + "Result: Hmmm, something strange🤔"
			}
			msg += "\n"
			msg += "\n" + "Make your choice again👇🏻👇🏻👇🏻"
		} else {
			msg += "\n" + "Lets play a round! Make your choice:"
			for _, ch := range hs.playUseCase.GetAllChoices() {
				msg += "\n" + makeChoiceLine(ch, true)
			}
		}
		msg += "```"
		return tgc.Send(msg, tg.ModeMarkdownV2, makeGoRoundReplyMarkup(tgc.Bot(), hs.playUseCase.GetAllChoices()))
	}
}

var allChoiceEmojies = map[entity.ChoiceID]string{
	entity.Rock:     "🗿",
	entity.Scissors: "✂️",
	entity.Paper:    "📃",
	entity.Lizard:   "🦎",
	entity.Spock:    "🖖🏻",
}

func getEmoji(chid entity.ChoiceID) string {
	return allChoiceEmojies[chid]
}

func toUpperFirstLetter(str string) string {
	rns := []rune(str)
	return strings.ToTitle(string(rns[:1])) + string(rns[1:])
}

func makePlayBtn(ch *entity.Choice) tg.Btn {
	return tg.Btn{
		Unique: "play",
		Text:   getEmoji(ch.ID) + toUpperFirstLetter(ch.Name),
		Data:   strconv.Itoa(int(ch.ID)),
	}
}

func makeChoiceLine(ch *entity.Choice, withNum bool) string {
	if withNum {
		return fmt.Sprintf("[%d] %s %s", ch.ID, getEmoji(ch.ID), toUpperFirstLetter(ch.Name))
	} else {
		return fmt.Sprintf("%s %s", getEmoji(ch.ID), toUpperFirstLetter(ch.Name))
	}
}

func makeGoRoundReplyMarkup(tgBot *tg.Bot, allChoices map[entity.ChoiceID]*entity.Choice) *tg.ReplyMarkup {
	tgReplyMarkup := tgBot.NewMarkup()

	tgReplyMarkup.Inline(
		tgReplyMarkup.Row(
			makePlayBtn(allChoices[entity.Rock]),
			makePlayBtn(allChoices[entity.Scissors]),
			makePlayBtn(allChoices[entity.Paper]),
		),
		tgReplyMarkup.Row(
			makePlayBtn(allChoices[entity.Lizard]),
			makePlayBtn(allChoices[entity.Spock]),
		),
	)
	return tgReplyMarkup
}

var actWords = map[entity.ChoiceID]map[entity.ChoiceID]string{
	entity.Scissors: {
		entity.Paper:  "cuts",        // Scissors cuts Paper
		entity.Lizard: "decapitates", // Scissors decapitates Lizard
	},
	entity.Paper: {
		entity.Rock:  "covers",    // Paper covers Rock
		entity.Spock: "disproves", // Paper disproves Spock
	},
	entity.Rock: {
		entity.Lizard:   "crushes",                    // Rock crushes Lizard
		entity.Scissors: "crushes (as it always has)", // Rock crushes Scissors (as it always has)
	},
	entity.Lizard: {
		entity.Spock: "poisons", // Lizard poisons Spock
		entity.Paper: "eats",    // Lizard eats Paper
	},
	entity.Spock: {
		entity.Scissors: "smashes",   // Spock smashes Scissors
		entity.Rock:     "vaporizes", // Spock vaporizes Rock
	},
}
