package ihttp

import (
	"log"
	"os"

	"github.com/swaggo/swag"
)

func RegisterSwagger(swaggerFilePath string) {
	already := swag.GetSwagger(swag.Name)
	if already == nil {
		swag.Register(swag.Name, &LocalFileSystemSwagger{
			LocalFilePath: swaggerFilePath,
		})
	}
}

type LocalFileSystemSwagger struct {
	LocalFilePath string
}

func (s *LocalFileSystemSwagger) ReadDoc() string {
	bytes, err := os.ReadFile(s.LocalFilePath)
	if err != nil {
		log.Fatal(err)
	}
	return string(bytes)
}
