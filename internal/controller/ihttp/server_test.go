package ihttp_test

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"

	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/app"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/config"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/controller/igrpc/gen"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/controller/ihttp"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/entity"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/infra/randomizer"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/usecase"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/pkg/kztest"
)

func makeHTTPHandler(t *testing.T, ctx context.Context, randomizer usecase.Randomizer) (ret http.Handler) {

	kztest.ChangeCurrentWorkDir(2)

	cfg, err := config.NewAPIConfig(ctx, "")
	require.NoError(t, err)

	if randomizer == nil {
		randomizer, err = app.MakeRandomizer(ctx, cfg.BaseConfig)
		require.NoError(t, err)
	}

	grpcServer, restServer, err := app.MakeAPIServers(ctx, cfg, randomizer)
	require.NoError(t, err)

	_ = grpcServer

	return restServer.Handler
}

func Test_GetAllChoicesEndpoint(t *testing.T) {
	ctx := context.Background()
	httpHandler := makeHTTPHandler(t, ctx, nil)

	target := "/api/choices"
	tkits := []kztest.HTTPTestKit{
		{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodGet,
				Target: target,
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusOK,
				BodyCheckFunc: func(t *testing.T, respBody string, tkit *kztest.HTTPTestKit) bool {
					var respData []*entity.Choice
					err := json.Unmarshal([]byte(respBody), &respData)
					require.NoError(t, err)

					validChoices := make([]*entity.Choice, 0, len(entity.AllChoices))
					for _, validChoice := range entity.AllChoices {
						validChoices = append(validChoices, validChoice)
					}
					return assert.ElementsMatch(t, validChoices, respData)
				},
			},
		},
		{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodPost,
				Target: target,
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusBadRequest,
			},
		},
		{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodGet,
				Target: target,
				Headers: map[string]string{
					"Content-Type": ihttp.TextPlain,
				},
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusOK,
			},
		},
		{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodGet,
				Target: "/" + target,
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusOK,
			},
		},
	}

	kztest.RunHTTPTests(t, httpHandler, tkits)
}

func Test_GetRandomChoiceEndpoint(t *testing.T) {
	target := "/api/choice"

	ctx := context.Background()
	staticRandomizer := randomizer.NewStaticRandomizer(0)
	httpHandler := makeHTTPHandler(t, ctx, staticRandomizer)

	makeSuccHTTPTestKitFunc := func(staticChoiceID entity.ChoiceID) kztest.HTTPTestKit {
		return kztest.HTTPTestKit{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodGet,
				Target: target,
			},
			BeforePerformRequestFunc: func(t *testing.T, req *http.Request, tkit *kztest.HTTPTestKit) {
				staticRandomizer.UpdateNumber(uint(staticChoiceID - 1))
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusOK,
				BodyCheckFunc: func(t *testing.T, respBody string, tkit *kztest.HTTPTestKit) bool {
					var respData *entity.Choice
					err := json.Unmarshal([]byte(respBody), &respData)
					require.NoError(t, err)

					staticChoice, err := entity.FindChoice(staticChoiceID)
					require.NoError(t, err)

					return assert.Exactly(t, staticChoice, respData)
				},
			},
		}
	}
	tkits := []kztest.HTTPTestKit{
		{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodPost,
				Target: target,
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusBadRequest,
			},
		},
		{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodGet,
				Target: target,
				Headers: map[string]string{
					"Content-Type": ihttp.TextPlain,
				},
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusOK,
			},
		},
		{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodGet,
				Target: "/" + target,
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusOK,
			},
		},
		makeSuccHTTPTestKitFunc(entity.Rock),
		makeSuccHTTPTestKitFunc(entity.Lizard),
		makeSuccHTTPTestKitFunc(entity.Spock),
		makeSuccHTTPTestKitFunc(entity.Scissors),
		makeSuccHTTPTestKitFunc(entity.Paper),
	}

	kztest.RunHTTPTests(t, httpHandler, tkits)
}

func Test_PlayEndpoint(t *testing.T) {
	target := "/api/play"

	ctx := context.Background()
	staticRandomizer := randomizer.NewStaticRandomizer(0)
	httpHandler := makeHTTPHandler(t, ctx, staticRandomizer)

	makeSuccHTTPTestKitFunc := func(playerChoiceID entity.ChoiceID, computerChoiceID entity.ChoiceID, resultVal entity.ResultVal) kztest.HTTPTestKit {
		return kztest.HTTPTestKit{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodPost,
				Target: target,
				Headers: map[string]string{
					"Content-Type": ihttp.ApplicationJSONCharsetUTF8,
				},
				Body: fmt.Sprintf(`{"player":%d}`, playerChoiceID),
			},
			BeforePerformRequestFunc: func(t *testing.T, req *http.Request, tkit *kztest.HTTPTestKit) {
				staticRandomizer.UpdateNumber(uint(computerChoiceID - 1))
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusOK,
				BodyCheckFunc: func(t *testing.T, respBody string, tkit *kztest.HTTPTestKit) bool {
					var respData *gen.PlayResponse
					err := json.Unmarshal([]byte(respBody), &respData)
					require.NoError(t, err)

					result, err := entity.FindResult(resultVal)
					require.NoError(t, err)

					return assert.Exactly(t,
						&gen.PlayResponse{
							Player:   gen.ChoiceID(playerChoiceID),
							Computer: gen.ChoiceID(computerChoiceID),
							Result:   int32(result.Val),
							Results:  result.Name,
						},
						respData,
					)
				},
			},
		}
	}
	tkits := []kztest.HTTPTestKit{
		makeSuccHTTPTestKitFunc(entity.Lizard, entity.Paper, entity.Win),
		makeSuccHTTPTestKitFunc(entity.Paper, entity.Lizard, entity.Lose),
		makeSuccHTTPTestKitFunc(entity.Paper, entity.Paper, entity.Tie),
		makeSuccHTTPTestKitFunc(entity.Lizard, entity.Lizard, entity.Tie),

		makeSuccHTTPTestKitFunc(entity.Rock, entity.Lizard, entity.Win),
		makeSuccHTTPTestKitFunc(entity.Lizard, entity.Spock, entity.Win),
		makeSuccHTTPTestKitFunc(entity.Spock, entity.Scissors, entity.Win),
		makeSuccHTTPTestKitFunc(entity.Spock, entity.Scissors, entity.Win),
		makeSuccHTTPTestKitFunc(entity.Scissors, entity.Paper, entity.Win),
		makeSuccHTTPTestKitFunc(entity.Paper, entity.Rock, entity.Win),
		{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodGet,
				Target: target,
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusBadRequest,
			},
		},
		{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodPost,
				Target: target,
				Body:   fmt.Sprintf(`{"player":%d}`, entity.Rock),
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusUnsupportedMediaType,
			},
		},
		{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodPost,
				Target: target,
				Headers: map[string]string{
					"Content-Type": ihttp.ApplicationJSON,
				},
				Body: fmt.Sprintf(`{"player":%d}`, entity.Rock),
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusUnsupportedMediaType,
			},
		},
		{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodPost,
				Target: target,
				Headers: map[string]string{
					"Content-Type": ihttp.ApplicationJSONCharsetUTF8,
				},
				Body: fmt.Sprintf(`{"player":%d}`, entity.Rock),
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusOK,
			},
		},
	}

	kztest.RunHTTPTests(t, httpHandler, tkits)
}

func Test_AnyOtherEndpoints(t *testing.T) {
	ctx := context.Background()
	httpHandler := makeHTTPHandler(t, ctx, nil)

	tkits := []kztest.HTTPTestKit{
		{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodGet,
				Target: "/sadga",
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusBadRequest,
			},
		},
		{
			Request: kztest.HTTPTestKitRequest{
				Method: http.MethodPost,
				Target: "/sadga",
			},
			Response: kztest.HTTPTestKitResponse{
				StatusCode: http.StatusBadRequest,
			},
		},
	}

	kztest.RunHTTPTests(t, httpHandler, tkits)
}
