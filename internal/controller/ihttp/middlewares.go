package ihttp

import (
	"net/http"
	"path"

	"github.com/go-chi/chi/v5/middleware"
)

func AllowContentCharsetMiddleware(charsets ...string) func(next http.Handler) http.Handler {
	chiMiddleware := middleware.ContentCharset(charsets...)

	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {

			// skip check for empty content body
			if r.ContentLength == 0 {
				next.ServeHTTP(w, r)
				return
			}

			chiMiddleware(next).ServeHTTP(w, r)
		})
	}
}

func CleanURLPathMiddleware() func(next http.Handler) http.Handler {
	return func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			res := path.Clean(r.URL.Path)
			if res == "" || res == "." {
				res = "/"
			}
			r.URL.Path = res

			next.ServeHTTP(w, r)
		})
	}
}
