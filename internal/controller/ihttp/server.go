package ihttp

import (
	"net/http"
	"strings"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"github.com/grpc-ecosystem/grpc-gateway/v2/runtime"
	"github.com/rs/zerolog/log"
	httpSwagger "github.com/swaggo/http-swagger"
	// Swagger docs.
)

const (
	ContentTypeHeaderName      = "Content-Type"
	UTF8                       = "UTF-8"
	TextHTML                   = "text/html"
	TextPlain                  = "text/plain"
	ApplicationJSON            = "application/json"
	TextPlainCharsetUTF8       = "text/plain;charset=UTF-8"
	ApplicationJSONCharsetUTF8 = "application/json;charset=UTF-8"
)

func RegisterHandlersCHI(
	rootChiRouter chi.Router,
	gwMux *runtime.ServeMux,
	allowedCORSOrigins string,
	swaggerFilePath string,
) {

	rootChiRouter.Use(CleanURLPathMiddleware())
	rootChiRouter.Use(AllowContentCharsetMiddleware(UTF8))
	rootChiRouter.Use(middleware.AllowContentType(ApplicationJSON))

	rootChiRouter.Use(cors.Handler(cors.Options{
		AllowedOrigins: strings.Fields(strings.ReplaceAll(allowedCORSOrigins, ",", " ")),
		AllowedMethods: []string{http.MethodGet, http.MethodPost},
		AllowedHeaders: []string{"Accept", "Content-Type"},
		MaxAge:         300, // Maximum value not ignored by any of major browsers
	}))

	retAPIVersionHandlerFunc := func(w http.ResponseWriter, req *http.Request) {
		req.URL.Path = "/api/version"
		gwMux.ServeHTTP(w, req)
	}
	rootChiRouter.Get("/", retAPIVersionHandlerFunc)
	rootChiRouter.Get("/api", retAPIVersionHandlerFunc)
	rootChiRouter.Get("/api/", retAPIVersionHandlerFunc)

	if swaggerFilePath != "" {
		RegisterSwagger(swaggerFilePath)
		rootChiRouter.Get("/swagger/*", httpSwagger.Handler())
	}

	for _, method := range []string{http.MethodGet, http.MethodPost} {
		rootChiRouter.Method(method, "/api/*", gwMux)
	}

	rootChiRouter.HandleFunc("/*", func(w http.ResponseWriter, req *http.Request) {
		log.Warn().Msgf("UNEXPECTED: %s %q\n", req.Method, req.URL.Path)
		http.Error(w, "Bad Request", http.StatusBadRequest)
	})
}
