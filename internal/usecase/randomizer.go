package usecase

import "context"

type Randomizer interface {
	// GetRandomNumber returns, as an uint, a non-negative pseudo-random number in the half-open interval [0,n).
	//GetRandomNumber(n uint) (uint, error)
	GetRandomNumber(ctx context.Context, n uint) (uint, error)
}
