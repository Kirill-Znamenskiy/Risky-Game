package usecase

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/entity"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/infra/randomizer"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/pkg/kztest"
)

func Test_CalcResult(t *testing.T) {
	staticRandomizer := randomizer.NewStaticRandomizer(0)
	playUseCase := NewPlayUseCase(staticRandomizer)
	prvPlayUseCaseImpl := playUseCase.(*prvPlayUseCaseImplementation)

	kztest.RunTests(t, prvPlayUseCaseImpl.CalcResult, []kztest.TestKit{
		{Arg1: entity.AllChoices[entity.Rock], Arg2: entity.AllChoices[entity.Lizard], Result1: entity.AllResults[entity.Win], Result2: assert.NoError},
		{Arg1: entity.AllChoices[entity.Rock], Arg2: entity.AllChoices[entity.Lizard], Result1: entity.AllResults[entity.Win], Result2: assert.NoError},
		{Arg1: entity.AllChoices[entity.Rock], Arg2: entity.AllChoices[entity.Spock], Result1: entity.AllResults[entity.Lose], Result2: assert.NoError},
		{Arg1: entity.AllChoices[entity.Paper], Arg2: entity.AllChoices[entity.Rock], Result1: entity.AllResults[entity.Win], Result2: assert.NoError},
		{Arg1: entity.AllChoices[entity.Lizard], Arg2: entity.AllChoices[entity.Paper], Result1: entity.AllResults[entity.Win], Result2: assert.NoError},
		{Arg1: entity.AllChoices[entity.Scissors], Arg2: entity.AllChoices[entity.Lizard], Result1: entity.AllResults[entity.Win], Result2: assert.NoError},
	})
}
