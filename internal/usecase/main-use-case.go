package usecase

type MainUseCase interface {
	GetVersion() string
}

type prvMainUseCaseImplementation struct {
	version string
}

func NewMainUseCase(version string) (ret MainUseCase) {
	return &prvMainUseCaseImplementation{
		version: version,
	}
}

func (this *prvMainUseCaseImplementation) GetVersion() string {
	return this.version
}
