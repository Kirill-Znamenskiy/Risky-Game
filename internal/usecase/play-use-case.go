package usecase

import (
	"context"

	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/entity"
)

type PlayUseCase interface {
	GetAllChoices() map[entity.ChoiceID]*entity.Choice
	GetRandomChoice(ctx context.Context) (randomChoice *entity.Choice, err error)
	Play(ctx context.Context, playerChoiceID entity.ChoiceID) (playerChoice *entity.Choice, computerChoice *entity.Choice, result *entity.Result, err error)
}

type prvPlayUseCaseImplementation struct {
	randomizer Randomizer
}

func NewPlayUseCase(randomizer Randomizer) (ret PlayUseCase) {
	return &prvPlayUseCaseImplementation{
		randomizer: randomizer,
	}
}

func (_ *prvPlayUseCaseImplementation) GetAllChoices() map[entity.ChoiceID]*entity.Choice {
	return entity.AllChoices
}

func (this *prvPlayUseCaseImplementation) GetRandomChoice(ctx context.Context) (randomChoice *entity.Choice, err error) {
	chid, err := this.randomizer.GetRandomNumber(ctx, uint(len(entity.AllChoices)))
	if err != nil {
		return
	}
	randomChoice = entity.AllChoices[entity.ChoiceID(chid+1)]
	return randomChoice, nil
}

func (this *prvPlayUseCaseImplementation) Play(ctx context.Context, playerChoiceID entity.ChoiceID) (playerChoice *entity.Choice, computerChoice *entity.Choice, result *entity.Result, err error) {

	playerChoice, err = entity.FindChoice(playerChoiceID)
	if err != nil {
		return
	}

	computerChoice, err = this.GetRandomChoice(ctx)
	if err != nil {
		return
	}

	result, err = this.CalcResult(playerChoice, computerChoice)
	if err != nil {
		return
	}

	return
}

// CalcResult calculate duel result for ch1 side.
func (_ *prvPlayUseCaseImplementation) CalcResult(ch1 *entity.Choice, ch2 *entity.Choice) (result *entity.Result, err error) {
	resultV := entity.Tie
	if ch1.ID != ch2.ID {

		res := int(ch1.ID) - int(ch2.ID)
		correction := entity.Lose // same as -1
		if res < 0 {
			correction = entity.Win // same as 1
		}
		if res%2 == 0 {
			resultV = entity.Lose * correction
		} else {
			resultV = entity.Win * correction
		}

	}
	return entity.FindResult(resultV)
}
