package config

import (
	"context"
	"time"

	"gitlab.com/Kirill-Znamenskiy/kzconfig"
)

const (
	EnvLOCAL = "LOCAL"
	EnvDEV   = "DEV"
	EnvPROD  = "PROD"
)

type BaseConfig struct {
	ENV                       string        `env:"ENV,required" flag:"env e" desc:"(env ENV) environment: LOCAL,DEV or PROD"`
	GitShowVersion            string        `env:"GIT_SHOW_VERSION" flat:"git-show-version" desc:"(env GIT_SHOW_VERSION) app version in form of git show command"`
	ExternalRandomizerURL     string        `env:"EXTERNAL_RANDOMIZER_URL" flag:"external-randomizer-url" desc:"(env EXTERNAL_RANDOMIZER_URL) external randomizer url"`
	ExternalRandomizerTimeout time.Duration `env:"EXTERNAL_RANDOMIZER_TIMEOUT,default=3s" flag:"external-randomizer-timeout" desc:"(env EXTERNAL_RANDOMIZER_TIMEOUT) external randomizer timeout"`
}

func (cfg *BaseConfig) IsLocalEnv() bool {
	return cfg.ENV == EnvLOCAL
}

// NewAPIConfig returns new api config.
func NewAPIConfig(ctx context.Context, prvGitShowVersion string) (ret *APIConfig, err error) {
	ret = new(APIConfig)

	err = kzconfig.LoadConfigFromEnv(ctx, ret,
		".env.DEFAULT.env",
		".env."+kzconfig.GetEnvValue("ENV", "LOCAL")+".env",
	)
	if err != nil {
		return
	}

	err = kzconfig.ParseConfigFlags(ret)
	if err != nil {
		return
	}

	if prvGitShowVersion != "" {
		ret.GitShowVersion = prvGitShowVersion
	}
	return
}

// NewTgBotConfig returns new api config.
func NewTgBotConfig(ctx context.Context, prvGitShowVersion string) (ret *TgBotConfig, err error) {
	ret = new(TgBotConfig)

	err = kzconfig.LoadConfigFromEnv(ctx, ret,
		".env.DEFAULT.env",
		".env."+kzconfig.GetEnvValue("ENV", "LOCAL")+".env",
	)
	if err != nil {
		return
	}

	err = kzconfig.ParseConfigFlags(ret)
	if err != nil {
		return
	}

	if prvGitShowVersion != "" {
		ret.GitShowVersion = prvGitShowVersion
	}
	return
}
