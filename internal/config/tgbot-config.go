package config

import (
	"time"
)

type TgBotConfig struct {
	*BaseConfig
	Token       string        `env:"TG_BOT_API_TOKEN,required" flag:"token t" desc:"(env TG_BOT_API_TOKEN) telegram bot access token"`
	PollTimeout time.Duration `env:"TG_BOT_POLL_TIMEOUT,default=11s" flag:"poll-timeout" desc:"(env TG_BOT_POLL_TIMEOUT) telegram bot poll timeout"`
}
