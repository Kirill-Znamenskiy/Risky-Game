package config

type APIConfig struct {
	*BaseConfig
	ServerAddress      string `env:"SERVER_ADDRESS,default=localhost:8080" flag:"server-address a" desc:"(env SERVER_ADDRESS) server address"`
	AllowedCORSOrigins string `env:"ALLOWED_CORS_ORIGINS,default=nobody" flag:"allowed-cors-origins" desc:"(env ALLOWED_CORS_ORIGINS) allowed origins, in terms of CORS"`
}
