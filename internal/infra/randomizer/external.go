package randomizer

import (
	"context"
	"encoding/json"
	"errors"
	"io"
	"net/http"
	neturl "net/url"
	"time"
)

type ExternalRandomizer struct {
	url        *neturl.URL
	httpClient *http.Client
}

func NewExternalRandomizer(url string, timeout time.Duration) (*ExternalRandomizer, error) {
	urlParsed, err := neturl.Parse(url)
	if err != nil {
		return nil, err
	}
	return &ExternalRandomizer{
		url: urlParsed,
		httpClient: &http.Client{
			Timeout: timeout,
		},
	}, nil
}

func (er *ExternalRandomizer) GetRandomNumber(ctx context.Context, n uint) (ret uint, err error) {
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, er.url.String(), nil)
	if err != nil {
		return 0, err
	}
	resp, err := er.httpClient.Do(req)
	if err != nil {
		return 0, err
	}
	defer resp.Body.Close()

	respBodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		return 0, err
	}

	respData := new(struct {
		RandomNumber int `json:"random_number"`
	})
	err = json.Unmarshal(respBodyBytes, respData)
	if err != nil {
		return 0, err
	}

	if respData.RandomNumber < 0 {
		return 0, errors.New("external random number < 0")
	}

	ret = uint(respData.RandomNumber) % n
	return ret, err
}

func (er *ExternalRandomizer) MarshalJSON() (ret []byte, err error) {
	return json.Marshal(map[string]map[string]string{"ExternalRandomizer": {
		"url":     er.url.String(),
		"timeout": er.httpClient.Timeout.String(),
	}})
}
