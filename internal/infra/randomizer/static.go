package randomizer

import (
	"context"
	"encoding/json"
	"fmt"
)

type StaticRandomizer struct {
	n uint
}

func NewStaticRandomizer(n uint) *StaticRandomizer {
	return &StaticRandomizer{
		n: n,
	}
}

func (sr *StaticRandomizer) GetRandomNumber(_ context.Context, n uint) (uint, error) {
	return sr.n % n, nil
}

func (sr *StaticRandomizer) UpdateNumber(n uint) {
	sr.n = n
}

func (sr *StaticRandomizer) MarshalJSON() (ret []byte, err error) {
	return json.Marshal(map[string]map[string]string{"StaticRandomizer": {
		"value": fmt.Sprintf("%d", sr.n),
	}})
}
