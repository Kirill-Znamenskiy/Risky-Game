package randomizer

import (
	"context"
	"encoding/json"
	"math/rand"
	"time"
)

type MathRandomizer struct {
}

func NewMathRandomizer() *MathRandomizer {
	rand.Seed(time.Now().UnixNano())
	return new(MathRandomizer)
}

func (mr *MathRandomizer) GetRandomNumber(_ context.Context, n uint) (uint, error) {
	return uint(rand.Intn(int(n))), nil
}

func (mr *MathRandomizer) MarshalJSON() (ret []byte, err error) {
	return json.Marshal(map[string]map[string]string{"MathRandomizer": {}})
}
