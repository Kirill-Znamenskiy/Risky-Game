// Package app configures and runs application.
package app

import (
	"context"
	"fmt"
	"net"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/rs/zerolog/log"
	"github.com/soheilhy/cmux"

	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/config"
)

// RunAPI run API.
func RunAPI(ctx context.Context, cfg *config.APIConfig) (err error) {

	randomizer, err := MakeRandomizer(ctx, cfg.BaseConfig)
	if err != nil {
		return fmt.Errorf("failed to create randomizer: %w", err)
	}

	grpcServer, restServer, err := MakeAPIServers(ctx, cfg, randomizer)
	if err != nil {
		return err
	}

	listener, err := net.Listen("tcp", cfg.ServerAddress)
	if err != nil {
		return fmt.Errorf("failed to start tcp listen at %q: %w", cfg.ServerAddress, err)
	}

	m := cmux.New(listener)
	grpcListener := m.Match(cmux.HTTP2HeaderField("Content-Type", "application/grpc"))
	restListener := m.Match(cmux.Any())

	finishedCh := make(chan error, 3)

	go func() {
		log.Info().Msgf("Run GRPC serve")
		finishedCh <- grpcServer.Serve(grpcListener)
	}()

	go func() {
		log.Info().Msgf("Run REST serve")
		finishedCh <- restServer.Serve(restListener)
	}()

	go func() {
		log.Info().Msgf("Run CMUX serve")
		finishedCh <- m.Serve()
	}()

	funcGracefulStop := func() {
		log.Info().Msgf("Now graceful stop GRPC server")
		grpcServer.GracefulStop()

		log.Info().Msgf("Now graceful stop REST server")
		{
			ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
			defer cancel()

			err = restServer.Shutdown(ctx)
			if err != nil {
				log.Error().Err(err).Msgf("Error while restServer.Shutdown")
			}
		}

		log.Info().Msgf("Now graceful stop CMUX server")
		m.Close()
	}

	funcHardStop := func() {
		log.Info().Msgf("Now hard stop GRPC server")
		grpcServer.Stop()

		log.Info().Msgf("Now hard stop REST server")
		err = restServer.Close()
		if err != nil {
			log.Error().Err(err).Msgf("Error while restServer.Close")
		}

		log.Info().Msgf("Now hard stop continuous accrual")
		m.Close()
	}

	// Waiting signal
	forKillSignalsCh := make(chan os.Signal, 1)
	signal.Notify(forKillSignalsCh, os.Kill)
	forInterruptSignalsCh := make(chan os.Signal, 1)
	signal.Notify(forInterruptSignalsCh, os.Interrupt, syscall.SIGTERM)

	select {
	case s := <-forKillSignalsCh:
		log.Error().Msgf("Accepted kill signal: %q", s.String())
		funcHardStop()
	case s := <-forInterruptSignalsCh:
		log.Error().Msgf("Accepted interrupt signal: %q", s.String())
		funcGracefulStop()
	case err = <-finishedCh:
		log.Error().Err(err).Msgf("Somebody finished with error!")
		funcGracefulStop()
	}

	return nil
}
