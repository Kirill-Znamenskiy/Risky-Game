package app

import (
	"context"
	"fmt"
	"net/http"

	"github.com/go-chi/chi/v5"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"

	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/config"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/controller/igrpc"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/controller/ihttp"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/infra/randomizer"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/usecase"
)

func MakeRandomizer(_ context.Context, cfg *config.BaseConfig) (usecase.Randomizer, error) {
	if cfg.ExternalRandomizerURL == "" {
		return randomizer.NewMathRandomizer(), nil
	} else {
		return randomizer.NewExternalRandomizer(cfg.ExternalRandomizerURL, cfg.ExternalRandomizerTimeout)
	}
}

func MakeAPIServers(ctx context.Context, cfg *config.APIConfig, randomizer usecase.Randomizer) (grpcServer *grpc.Server, restServer *http.Server, err error) {

	grpcServer, gwMux, err := igrpc.NewServer(ctx,
		usecase.NewMainUseCase(cfg.GitShowVersion),
		usecase.NewPlayUseCase(randomizer),
	)
	if err != nil {
		return nil, nil, fmt.Errorf("failed to create grpc server: %w", err)
	}
	if cfg.IsLocalEnv() {
		// Register reflection service on gRPC server.
		reflection.Register(grpcServer)
	}

	swaggerFilePath := "./internal/controller/igrpc/gen/risky-game.swagger.json"

	chiRouter := chi.NewRouter()
	ihttp.RegisterHandlersCHI(chiRouter, gwMux, cfg.AllowedCORSOrigins, swaggerFilePath)
	restServer = &http.Server{
		Handler: chiRouter,
	}

	return grpcServer, restServer, nil
}
