// Package app configures and runs application.
package app

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/rs/zerolog/log"

	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/config"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/controller/tgbot"
	"GitLab.com/Kirill-Znamenskiy/Risky-Game/internal/usecase"
)

// RunTgBot run Telegram Bot.
func RunTgBot(ctx context.Context, cfg *config.TgBotConfig) (err error) {

	randomizer, err := MakeRandomizer(ctx, cfg.BaseConfig)
	if err != nil {
		return fmt.Errorf("failed to create randomizer: %w", err)
	}

	tgBot, err := tgbot.NewTgBot(ctx,
		cfg.Token, cfg.PollTimeout,
		usecase.NewMainUseCase(cfg.GitShowVersion),
		usecase.NewPlayUseCase(randomizer),
	)
	if err != nil {
		return fmt.Errorf("failed to create new tg bot: %w", err)
	}

	tgBot.Start()

	finishedCh := make(chan error, 3)

	go func() {
		log.Info().Msgf("Run TgBot serve")
		tgBot.Start()
		finishedCh <- fmt.Errorf("TgBot unexpected finished")
	}()

	funcStop := func() {
		log.Info().Msgf("Now stop TgBot serve")
		tgBot.Stop()
	}

	// Waiting signal
	forKillSignalsCh := make(chan os.Signal, 1)
	signal.Notify(forKillSignalsCh, os.Kill)
	forInterruptSignalsCh := make(chan os.Signal, 1)
	signal.Notify(forInterruptSignalsCh, os.Interrupt, syscall.SIGTERM)

	select {
	case s := <-forKillSignalsCh:
		log.Error().Msgf("Accepted kill signal: %q", s.String())
		funcStop()
	case s := <-forInterruptSignalsCh:
		log.Error().Msgf("Accepted interrupt signal: %q", s.String())
		funcStop()
	case err = <-finishedCh:
		log.Error().Err(err).Msgf("Somebody finished with error!")
		funcStop()
	}

	return nil
}
