package entity

import "fmt"

type ResultVal int8

const (
	Lose ResultVal = iota - 1
	Tie
	Win
)

type Result struct {
	Val  ResultVal
	Name string
}

var AllResults = map[ResultVal]*Result{
	Tie:  {Val: Tie, Name: "Tie"},
	Win:  {Val: Win, Name: "Win"},
	Lose: {Val: Lose, Name: "Lose"},
}

func FindResult(rv ResultVal) (result *Result, err error) {
	result, ok := AllResults[rv]
	if !ok {
		return result, fmt.Errorf("invalid resultVal %d", rv)
	}
	return result, nil
}
