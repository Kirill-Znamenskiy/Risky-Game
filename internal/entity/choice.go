package entity

import "fmt"

type ChoiceID uint8

const (
	Rock ChoiceID = iota + 1
	Lizard
	Spock
	Scissors
	Paper
)

type Choice struct {
	ID   ChoiceID `json:"id"`
	Name string   `json:"name"`
}

var AllChoices = map[ChoiceID]*Choice{
	Rock:     {ID: Rock, Name: "Rock"},
	Lizard:   {ID: Lizard, Name: "Lizard"},
	Spock:    {ID: Spock, Name: "Spock"},
	Scissors: {ID: Scissors, Name: "Scissors"},
	Paper:    {ID: Paper, Name: "Paper"},
}

func FindChoice(chid ChoiceID) (choice *Choice, err error) {
	choice, ok := AllChoices[chid]
	if !ok {
		return choice, fmt.Errorf("invalid choiceID %d", chid)
	}
	return choice, nil
}
