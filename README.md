# Risky Game 🗿 ✂️ 📝 🦎 🖖

[<img alt="Risky-Game license" title="Risky-Game license" src="https://img.shields.io/github/license/go-chi/chi?style=for-the-badge"/>](/LICENSE)

## Description
This is a project about a really Risky Game "Rock Scissors Paper Lizard Spock"!


## Short Rules 📖
- Scissors cuts Paper
- Paper covers Rock
- Rock crushes Lizard
- Lizard poisons Spock
- Spock smashes Scissors
- Scissors decapitates Lizard
- Lizard eats Paper
- Paper disproves Spock
- Spock vaporizes Rock
- (and as it always has) Rock crushes Scissors

More detailed game rules you can find [here](http://www.samkass.com/theories/RPSSL.html)

## Demo
You can play a Risky Game right now - just send /start to [Telegram Bot](http://t.me/risky_game_bot) and enjoy!

Also you can play using [this UI](https://codechallenge.boohma.com). At Step 1 just specify demo backend API URL https://Risky-Game.Kirill.Znamenskiy.pw/api/ and enjoy! 
<br/>This demo stand allow any origins (in terms of CORS), so you can use it as backend for any appropriate UI.
<br/>It's important to mention that this demo stand use [this external randomizer](https://codechallenge.boohma.com/random).


## Usage

<details>
<summary>Requirements</summary>

You need to have [Go](https://go.dev) and [Git](https://git-scm.com) installed and configured on your machine.
You can install both, by following [this](https://go.dev/doc/install) and [this](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git) guides.
</details>

First of all clone this repository
```
git clone https://GitLab.com/Kirill-Znamenskiy-Projects/Risky-Game.git;
cd Risky-Game;
```

To run project tests
```
make test
```

### Usage from Scratch

To run api server with default options (at "localhost:8080", with internal math randomizer)
```
make run-api
```

To see all available options to run api server
```
make run-api-help
```

To run api server configured for [Boohma UI](https://codechallenge.boohma.com)
```
make run-api-boohma
```

To run api server at "localhost:8888", with external randomizer "https://codechallenge.boohma.com/random"
```
SERVER_ADDRESS="localhost:8888" \
EXTERNAL_RANDOMIZER_URL="https://codechallenge.boohma.com/random" \
make run-api
```


### Usage with Docker Compose

<details>
<summary>Requirements</summary>

You need to have [Docker](https://docker.com) and [Docker Compose](https://docs.docker.com/compose/) installed and configured on your machine.
<br/>You can install both, by following [this](https://docs.docker.com/engine/install/) and [this](https://docs.docker.com/compose/install/) guides.
</details>

All docker-compose services defined in ./docker-compose/docker-compose.yml file,
feel free to override it in [true way](https://docs.docker.com/compose/extends/#multiple-compose-files) 

<br/>

To start all docker-compose services in "default" environment (using .env.example.env file)
```
make dc-up
// or detached variant
make dc-up-detached
```

<br/>

To start all docker-compose services in "boohma" environment (using .env.boohma.env file),
this environment is fully configured for [Boohma UI](https://codechallenge.boohma.com)
```
make dc-boohma-up
// or detached variant
make dc-boohma-up-detached
```

<br/>

To start all docker-compose services in your own custom environment:
- create your own env file:
  ```
  cp ./docker-compose/.env.example.env ./docker-compose/.env.my-local.env
  ```
- set any env variable you need in `./docker-compose/.env.my-local.env`
- start containers: 
  ```
  ENV_FILE=./docker-compose/.env.my-local.env make dc-up
  // or detached variant
  ENV_FILE=./docker-compose/.env.my-local.env make dc-up-detached
  ```

### API Service endpoints
<details><summary>OK</summary>
Return OK, useful to check service health state.

GET /api/
<br/>
Response:
```
Content-Type: application/json
{
  “version”: string
}
```
</details>

<details><summary>Choices</summary>
Return all the choices that are usable for the UI.

GET /api/choices
<br/>
Response:
```
Content-Type: application/json
[{
  “id": integer, // (1-5)
  "name": "string" // (rock, paper, scissors, lizard, spock)
}]
```

</details>

<details><summary>Randome Choice</summary>
Return randomly generated choice.

GET /api/choice
<br/>
Response:
```
Content-Type: application/json
{
  “id": choice_id, // integer (1-5)
  "name": "string" // (rock, paper, scissors, lizard, spock)
}
```

</details>

<details><summary>Play</summary>
Play a round against a computer opponent.

Request:
<br/>
POST /api/play
```
Content-Type: application/json
{
  “player”: choice_id // integer (1-5)
}
```

Response:
```
Content-Type: application/json
{
  “player”: choice_id, // integer (1-5)
  “computer”: choice_id, // integer (1-5)
  "result": integer, // (-1 = lose, 0 = tie, 1 = win)
  "results": "string" // (win, lose, tie),
  
}
```
</details>


<details><summary>Version</summary>
Return current app version in form of git show.

GET /api/version
<br/>
Response:
```
Content-Type: application/json
{
  “version”: string
}
```
</details>

## License
Risky-Game is licensed under the [MIT License](/LICENSE).