# ==============================================================================
# Main commands
# ==============================================================================

# To run project tests
test:
	go test -cover ./...

# To run all cmd programs with default options
run:
	go run ./cmd/...

# To run api server with default options (at "localhost:8080", with internal math randomizer)
run-api:
	go run ./cmd/api/api.go

# To see all available options to run api server
run-api-help:
	go run ./cmd/api/api.go --help

# To run api server configured for https://codechallenge.boohma.com
run-api-boohma:
	ALLOWED_CORS_ORIGINS="*,https://codechallenge.boohma.com" \
	EXTERNAL_RANDOMIZER_URL="https://codechallenge.boohma.com/random" \
	go run ./cmd/api/api.go


run-tgbot:
	go run ./cmd/tgbot/tgbot.go

run-tgbot-help:
	go run ./cmd/tgbot/tgbot.go --help

# ==============================================================================
# Modules support commands
# ==============================================================================

mod-tidy:
	go mod tidy
	#go mod vendor

mod-deps-reset:
	git checkout -- go.mod
	go mod tidy
	#go mod vendor

mod-deps-upgrade:
	# go get $(go list -f '{{if not (or .Main .Indirect)}}{{.Path}}{{end}}' -m all)
	go get -u -t -d -v ./...
	go mod tidy
	#go mod vendor

mod-deps-cleancache:
	go clean -modcache




# ==============================================================================
# Docker-Compose commands
# ==============================================================================

dc-up:
	@bash -c ` \
		export MKFILE_DC_ENV_FILE="$${MKFILE_DC_ENV_FILE:-./docker-compose/.env.EXAMPLE.env}"; \
		export ENV_FILE="$${ENV_FILE:-$${MKFILE_DC_ENV_FILE}}"; \
		echo "Starting up docker-compose services with env file \"$${ENV_FILE}\""; \
		export GIT_SHOW="$$(git show --no-color --no-patch --decorate | head -n 1)"; \
		set -xe; \
		docker-compose \
		  --env-file $${ENV_FILE} \
		  --file ./docker-compose/docker-compose.yml \
		  up --build ${MKFILE_DC_DETACH} --remove-orphans \
		  -- nginx api tgbot; \
		set +xe; \
	`

dc-up-detached: MKFILE_DC_DETACH = --detach
dc-up-detached: dc-up

dc-boohma-up: MKFILE_DC_ENV_FILE = ./docker-compose/.env.BOOHMA.env
dc-boohma-up: dc-up

dc-boohma-up-detached: MKFILE_DC_ENV_FILE = ./docker-compose/.env.BOOHMA.env
dc-boohma-up-detached: dc-up-detached


dc-down:
	@bash -c ` \
		set -xe; \
		echo "Shutting down all docker-compose services"; \
		docker-compose \
		  --file ./docker-compose/docker-compose.yml \
		  down; \
		set +xe; \
	`



